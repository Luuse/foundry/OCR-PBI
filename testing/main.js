var testing, pad, inReload, inText, inScale, inPad, iframe

function scale(value){
	console.log(testing.style)
	testing.style.transform = 'scale(' + value + ')'
}

function rand(){
	return Math.floor((Math.random() * 10000) + 1)
}

function loadLetters(letters){
	var tabLetters = letters.split('')
	testing.innerHTML = ''
	tabLetters.forEach(function(item, i){
		var itemCode = item.charCodeAt(0)
		var ra = rand()
		console.log(tableChar[itemCode])
		var letter = tableChar[itemCode]

		testing.innerHTML += '<img src="../svg/' + letter[0] + '_' + letter[1] + '.svg?rand=' + ra + '" />' 
	})
}

document.addEventListener("DOMContentLoaded", (event) => {
	testing = document.querySelector('#testing')
	pad = document.querySelector('#pad')
	inReload = document.querySelector('#inReload')
	inText = document.querySelector('#inText')
	inScale = document.querySelector('#inScale')

	inReload.addEventListener('click', (event) => {
		loadLetters(inText.value)
	})

	inScale.addEventListener('change', (event) => {
		console.log(inScale.value)
		scale(inScale.value)
	})
})
