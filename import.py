import fontforge
import glob
import lxml.etree as ET


line_code = "charstart("
compositeChar = [192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 210, 211, 212, 213, 214, 217, 218, 219, 220, 224, 225, 226, 227, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 242, 243, 244, 249, 250, 251, 252, 268, 269, 282, 283, 327, 328, 344, 345, 352, 353, 338, 381, 382, 339, 366, 367]


def removeCadra(root, pattern):
    for child in root:
        if child.tag == '{http://www.w3.org/2000/svg}path':
            if child.attrib['style'].startswith(pattern):
                b = child
            try:
                root.remove(b)
            except:
                print('hoo')
    ET.dump(root)
    return ET.tostring(root, encoding='utf8', method='xml').decode()

def buildFont(svgFolder, fontname):
    SVGS = glob.glob(svgFolder)
    ff = fontforge.font()
    # ff.reencode(encoding-name, force])
    for SVG in SVGS:
        filename = SVG.split('/')[-1].split('.')[0]
        glyph_name, key = filename.split('__')

        if key == 'ps':
            mp_file = open("letters/"+glyph_name+".mp", "r")
            for ligne in mp_file:
                if line_code in ligne:
                    key = ligne.split("(")[-1].split(")")[-2]

        if key.isdigit() == True:
            with open(SVG, 'rb') as gp:
                treeLet = ET.parse(gp)
            rootLet = treeLet.getroot()
            gwidth = float(rootLet.get('width')) * (1000 / float(rootLet.get('height')))
            gclean = removeCadra(rootLet, 'stroke:rgb(100.000000%,0.000000%,0.000000%);')
            f = open('/tmp/'+key+'.svg', 'w')
            f.write(gclean)
            f.close()
            letter_char = ff.createChar(int(key))
            letter_char.importOutlines('/tmp/'+key+'.svg')
            # letter_char.removeOverlap()
            # letter_char.correctDirection()
            letter_char.width = int(gwidth)

    for letter_comp in compositeChar:
        print(letter_comp)
        glyphAcc = ff.createChar(letter_comp)
        # glyphAcc.addAnchorPoint('gpos_mark2base', 'basemark', 0, 0)
        glyphAcc.build()
        # glyphAcc.autoWidth()
        print("###################### yesss ") 
        print(dir(glyphAcc)) 
        print("######################") 
        print(glyphAcc)
        # glyphAcc.centerInWidth()


    ff.fontname = fontname 
    ff.familyname = fontname
    ff.generate('FINAL/'+fontname+'.otf')
    ff.generate('FINAL/'+fontname+'.ttf')
    ff.generate('FINAL/'+fontname+'.sfd')

if __name__ == '__main__':
    buildFont('svg/*.svg', 'ocr-75-premetrics')

