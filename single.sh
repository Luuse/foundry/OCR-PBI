#!/bin/sh

MOT=$1
# RELOAD=$2

  if [ $MOT == -all ]; then
    for LETTER_MP in letters/*.mp; do
			echo $LETTER_MP
      IFS=.
      set $LETTER_MP
      LETTER_=$1
      IFS=/
      set $LETTER_
      LETTER=$2
      IFS=
      echo '##########'
      echo $LETTER
      echo '##########'
      mpost -interaction=batchmode -s 'outputformat="svg"' $LETTER_MP
      # mv $LETTER.svg svg/$LETTER.svg
    done
  else
    for LETTER in $MOT; do
      mpost -interaction=batchmode -s 'outputformat="svg"' letters/$LETTER.mp
      echo $LETTER
      mv $LETTER*.svg svg/$LETTER.svg
    done
  fi

rm *.log
