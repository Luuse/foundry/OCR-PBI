# Ocr Pbi (metapost)
Ocr-PBI is a metapost programme font.

## Requierment
  
  * Metapost (texLive)
  * python Fontforge
  * Bash
  * Inkscape


## Licenses

### TTF/OTF/SFD
under SIL Open Font License (OFL)

### MP/EXE/PY/SVG
under the GPLv3

##
Antoine Gelgon
Lise Brosseau
2015/2016/2017
