import glob, os

svgs = glob.glob('svg/*.svg')



for svg in svgs:
    name, key = svg.split('/')[-1].replace('.svg', '').split('__')
    os.rename('letters-key/'+name+'.mp', 'letters-key/'+key+'.mp')
